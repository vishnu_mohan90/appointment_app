package com.indezire.appointment.interfaces

import com.indezire.appointment.models.MenuItem

interface IMainMenuClickListener {
    fun onMenuItemClicked(menuItem: MenuItem)
}