package com.indezire.appointment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.models.networking.SignupRequest
import com.indezire.appointment.models.networking.SignupResponse
import com.indezire.appointment.repositories.UserRepository

class SignUpViewModel: ViewModel() {
    var userName        = MutableLiveData("")
    var password        = MutableLiveData("")
    var emailId         = MutableLiveData("")
    var phoneNumber     = MutableLiveData("")
    var businessId      = MutableLiveData("")
    var confirmPassword = MutableLiveData("")

    var apiStatus       = MutableLiveData<ApiStatus>()
    var apiResponse     = MutableLiveData<SignupResponse>()

    fun signup() {
        val signupRequest = getSignupRequest()

        apiStatus.value = ApiStatus.LOADING
        UserRepository.signup(signupRequest) { isSuccess, response ->
            apiResponse.value = response
            if(isSuccess) {
                apiStatus.value = ApiStatus.SUCCESS
            } else {
                apiStatus.value = ApiStatus.FAILURE
            }
        }
    }

    private fun getSignupRequest(): SignupRequest {
        return SignupRequest(
            emailId.value ?: "",
            phoneNumber.value ?: "",
            userName.value ?: "",
            password.value ?: "")
    }
}