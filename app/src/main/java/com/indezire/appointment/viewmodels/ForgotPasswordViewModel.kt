package com.indezire.appointment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.models.networking.VerifyEmailRequest
import com.indezire.appointment.models.networking.VerifyEmailResponses
import com.indezire.appointment.repositories.UserRepository

class ForgotPasswordViewModel: ViewModel() {
    val email = MutableLiveData("")
    val apiStatus = MutableLiveData<ApiStatus>()
    val apiResponse = MutableLiveData<VerifyEmailResponses>()

    fun verifyEmail() {
        val verifyEmailRequest = VerifyEmailRequest(email.value ?: "")

        apiStatus.value = ApiStatus.LOADING
        UserRepository.verifyEmail(verifyEmailRequest) { isSuccess, response ->
            apiResponse.value = response
            if(isSuccess) {
                apiStatus.value = ApiStatus.SUCCESS
            } else {
                apiStatus.value = ApiStatus.FAILURE
            }
        }
    }
}