package com.indezire.appointment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.models.networking.ResetPasswordRequest
import com.indezire.appointment.models.networking.ResetPasswordResponse
import com.indezire.appointment.repositories.UserRepository

class ResetPasswordViewModel: ViewModel() {
    val otp             = MutableLiveData("")
    val password        = MutableLiveData("")
    val confirmPassword = MutableLiveData("")

    val apiStatus       = MutableLiveData<ApiStatus>()
    val apiResponse     = MutableLiveData<ResetPasswordResponse>()

    fun resetPassword(mailId: String) {
        val resetPasswordRequest = getResetPasswordRequest(mailId)
        UserRepository.resetPassword(resetPasswordRequest) { isSuccess, response ->
            apiResponse.value = response

            if(isSuccess) {
                apiStatus.value = ApiStatus.SUCCESS
            } else {
                apiStatus.value = ApiStatus.FAILURE
            }
        }
    }

    private fun getResetPasswordRequest(mailId: String): ResetPasswordRequest {
        return ResetPasswordRequest(mailId, otp.value ?: "", password.value ?: "")
    }
}