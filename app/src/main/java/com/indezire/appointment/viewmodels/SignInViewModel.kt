package com.indezire.appointment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.models.networking.LoginRequest
import com.indezire.appointment.models.networking.LoginResponse
import com.indezire.appointment.repositories.UserRepository

class SignInViewModel: ViewModel() {
    val username = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")
    val database = MutableLiveData<String>("appointment")
    val apiStatus = MutableLiveData<ApiStatus>()
    val loginResponse = MutableLiveData<LoginResponse>()

    fun login() {
        val loginRequest = getLoginRequest()

        apiStatus.value = ApiStatus.LOADING
        UserRepository.login(loginRequest) { isSuccess, response ->
            loginResponse.value = response
            if(isSuccess) {
                apiStatus.value = ApiStatus.SUCCESS
            } else {
                apiStatus.value = ApiStatus.FAILURE
            }
        }
    }

    private fun getLoginRequest(): LoginRequest {
        val loginRequest = LoginRequest()

        loginRequest.username = username.value ?: ""
        loginRequest.password = password.value ?: ""
        loginRequest.database = database.value ?: ""

        return loginRequest
    }
}