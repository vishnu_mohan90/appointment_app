package com.indezire.appointment.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.indezire.appointment.R
import com.indezire.appointment.adapters.MainMenuAdapter
import com.indezire.appointment.databinding.FragmentHomeBinding
import com.indezire.appointment.enums.MenuItemTypes
import com.indezire.appointment.helpers.DividerItemDecoration
import com.indezire.appointment.interfaces.IMainMenuClickListener
import com.indezire.appointment.models.MenuItem
import com.indezire.appointment.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : AbstractFragment(), IMainMenuClickListener {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var adapter: MainMenuAdapter
    private lateinit var rootView: View
    private lateinit var navController: NavHostFragment

    override fun initLayout(inflater: LayoutInflater, container: ViewGroup?): View {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val binding: FragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        rootView = binding.root
        binding.viewModel = homeViewModel
        binding.lifecycleOwner = this
        populateMenu()

//        val navHostFragment = fragmentManager?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
//        val navController = navHostFragment.navController

        return rootView
    }

    override fun initListeners(view: View) {

    }

    override fun initObservers() {

    }

    private fun populateMenu() {
        val menuItems = MenuItem.getMenuItems()
        adapter = MainMenuAdapter(requireContext(), menuItems, this)
        rootView.rvMainMenu.layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        rootView.rvMainMenu.addItemDecoration(DividerItemDecoration())
        rootView.rvMainMenu.adapter = adapter
    }

    override fun onMenuItemClicked(menuItem: MenuItem) {
        when(menuItem.type) {
            MenuItemTypes.MY_APPOINTMENTS -> {
                view?.findNavController()?.navigate(R.id.action_nav_home_to_nav_my_appointments)
            }

            MenuItemTypes.BOOK_APPOINTMENTS -> {
                view?.findNavController()?.navigate(R.id.action_nav_home_to_nav_book_appointments)
            }

            MenuItemTypes.ALERTS -> {
                view?.findNavController()?.navigate(R.id.action_nav_home_to_nav_alerts)
            }

            MenuItemTypes.LOGOUT -> {

            }
        }
    }
}