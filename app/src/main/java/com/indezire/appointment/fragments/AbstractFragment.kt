package com.indezire.appointment.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class AbstractFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = initLayout(inflater, container)
        initListeners(view)
        initObservers()
        return view
    }

    abstract fun initObservers()

    abstract fun initListeners(view: View)

    abstract fun initLayout(inflater: LayoutInflater, container: ViewGroup?): View
}