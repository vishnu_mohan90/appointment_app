package com.indezire.appointment.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.indezire.appointment.R
import com.indezire.appointment.viewmodels.BookAppointmentsViewModel

class BookAppointmentsFragment : Fragment() {

    private lateinit var slideshowViewModel: BookAppointmentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
            ViewModelProviders.of(this).get(BookAppointmentsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_book_appointments, container, false)
//        val textView: TextView = root.findViewById(R.id.text_slideshow)
//        slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })
        return root
    }
}