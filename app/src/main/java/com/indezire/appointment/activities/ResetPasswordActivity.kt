package com.indezire.appointment.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.indezire.appointment.R
import com.indezire.appointment.databinding.ActivityForgotPasswordBinding
import com.indezire.appointment.databinding.ActivityResetPasswordBinding
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.helpers.CommonUtils
import com.indezire.appointment.helpers.Constants
import com.indezire.appointment.helpers.Preferences
import com.indezire.appointment.viewmodels.ForgotPasswordViewModel
import com.indezire.appointment.viewmodels.ResetPasswordViewModel
import kotlinx.android.synthetic.main.activity_reset_password.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ResetPasswordActivity : AbstractActivity() {

    private lateinit var viewModel: ResetPasswordViewModel

    override fun initLayout() {
        val binding: ActivityResetPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)

        viewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        supportActionBar?.hide()
    }

    override fun initListeners() {
        tvSignUp.setOnClickListener {
            startSignUpActivity()
        }

        btReset.setOnClickListener {
            resetPassword()
        }
    }

    private fun resetPassword() {
        val emailId = Preferences.getInstance(this).getUserMailId()

        if(isValidPassword()) {
            viewModel.resetPassword(emailId)
        }
    }

    private fun isValidPassword(): Boolean {
        var isValid = true

        when {
            !viewModel.password.value.isValidPassword() -> {
                isValid = false
                etNewPassword.error = "Please enter a valid password"
                etNewPassword.requestFocus()
            }

            viewModel.confirmPassword.value != viewModel.password.value -> {
                isValid = false
                etConfirmPassword.error = "Password does not match"
                etConfirmPassword.requestFocus()
            }
        }

        return isValid
    }

    private fun startSignUpActivity() {
        val intent = Intent(this, SignUpActivity::class.java)

        startActivity(intent)
    }

    override fun initObservers() {
        viewModel.apiStatus.observe(this, Observer {
            handleAPIStatus(it)
        })

        viewModel.apiResponse.observe(this, Observer {

        })

        viewModel.password.observe(this, Observer {

        })
    }

    private fun handleAPIStatus(apiStatus: ApiStatus) {
        CommonUtils.processAPIStatus(apiStatus, this)

        if(apiStatus == ApiStatus.SUCCESS) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.apiResponse.value?.message) {
                Toast.makeText(this, "Redirecting to login screen", Toast.LENGTH_SHORT).show()
                openLoginScreen()
            }
        } else if (apiStatus == ApiStatus.FAILURE) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.apiResponse.value?.message)
        }
    }

    private fun openLoginScreen() {
        val intent = Intent(this, SignInActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun CharSequence?.isValidPassword(): Boolean {
        return this?.trim()?.isNotEmpty()!! && this.trim().length >= Constants.VALUE_PASSWORD_LENGTH
    }

    companion object {
        private const val TAG = "ResetPasswordActivity"
    }
}
