package com.indezire.appointment.activities

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.indezire.appointment.R
import com.indezire.appointment.databinding.ActivityForgotPasswordBinding
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.helpers.CommonUtils
import com.indezire.appointment.viewmodels.ForgotPasswordViewModel
import kotlinx.android.synthetic.main.activity_forgot_password.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ForgotPasswordActivity : AbstractActivity() {

    private lateinit var viewModel: ForgotPasswordViewModel

    override fun initLayout() {
        val binding: ActivityForgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        viewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        supportActionBar?.hide()
    }

    override fun initListeners() {
         btSend.setOnClickListener {
             verifyEmailAndProceed()
         }

        tvSignUp.setOnClickListener {
            startSignUpActivity()
        }
    }

    private fun verifyEmailAndProceed() {
        viewModel.verifyEmail()
    }

    private fun startSignUpActivity() {
        val intent = Intent(this, SignUpActivity::class.java)

        startActivity(intent)
    }

    private fun startResetPasswordActivity() {
        val intent = Intent(this, ResetPasswordActivity::class.java)

        startActivity(intent)
    }

    override fun initObservers() {
        viewModel.apiStatus.observe(this, Observer {
            handleAPIStatus(it)
        })
    }

    private fun handleAPIStatus(it: ApiStatus) {
        CommonUtils.processAPIStatus(it, this)

        if(it == ApiStatus.SUCCESS) {
            startResetPasswordActivity()
        } else if (it == ApiStatus.FAILURE) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.apiResponse.value?.message)
        }
    }

    companion object {
        private const val TAG = "ForgotPasswordActivity"
    }
}