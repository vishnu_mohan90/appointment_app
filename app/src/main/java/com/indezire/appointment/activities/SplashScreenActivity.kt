package com.indezire.appointment.activities

import android.content.Intent
import android.os.CountDownTimer
import com.indezire.appointment.BuildConfig
import com.indezire.appointment.R
import com.indezire.appointment.helpers.Preferences
import kotlinx.android.synthetic.main.activity_fullscreen.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SplashScreenActivity : AbstractActivity() {
    override fun initLayout() {
        setContentView(R.layout.activity_fullscreen)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.elevation = 0.0f

        tvAppName.text = BuildConfig.APP_NAME

        startTimer()
    }

    private fun startTimer() {
        val timer = object: CountDownTimer(0 * 1000, 1000) {
            override fun onFinish() {
                checkLoginStatusAndStartActivity()
            }

            override fun onTick(millisUntilFinished: Long) {

            }
        }

        timer.start()
    }

    private fun checkLoginStatusAndStartActivity() {
        val intent: Intent
        val preferences = Preferences.getInstance(this)
        val isLoggedIn = preferences.isLoggedIn()

        intent = if(isLoggedIn) {
            Intent(this, HomeScreenActivity::class.java)
        } else {
            Intent(this, SignInActivity::class.java)
        }

        startActivity(intent)
        finish()
    }

    override fun initListeners() {

    }

    override fun initObservers() {

    }
    companion object {
        private const val TAG = "FullscreenActivity"
    }
}