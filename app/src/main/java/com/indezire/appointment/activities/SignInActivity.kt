package com.indezire.appointment.activities

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.indezire.appointment.R
import com.indezire.appointment.databinding.ActivitySigninBinding
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.helpers.CommonUtils
import com.indezire.appointment.helpers.Constants
import com.indezire.appointment.helpers.Preferences
import com.indezire.appointment.viewmodels.SignInViewModel
import kotlinx.android.synthetic.main.activity_signin.*
import java.util.regex.Pattern

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SignInActivity : AbstractActivity() {
    private lateinit var viewModel: SignInViewModel

    override fun initLayout() {
        val binding: ActivitySigninBinding = DataBindingUtil.setContentView(this, R.layout.activity_signin)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.hide()

        viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    override fun initListeners() {
        tvSignUp.setOnClickListener {
            startSignUpActivity()
        }

        btSignIn.setOnClickListener {
            signIn()
        }

        tvForgotPassword.setOnClickListener {
            startForgotPasswordActivity()
        }
    }

    private fun signIn() {
        if(validateInputParams()) {
            viewModel.login()
        }
    }

    private fun validateInputParams(): Boolean {
        var isValid = true

        when {
            !viewModel.username.value?.isValidName()!! -> {
                isValid = false
                etUsername.error = "Please enter a valid username"
                etUsername.requestFocus()
            }

            !viewModel.password.value?.isValidPassword()!! -> {
                isValid = false
                etPassword.error = "Please enter a valid password"
                etPassword.requestFocus()
            }
        }

        return isValid
    }

    private fun startForgotPasswordActivity() {
        val intent = Intent(this, ForgotPasswordActivity::class.java)

        startActivity(intent)
    }

    private fun startHomeScreen() {
        val intent = Intent(this, HomeScreenActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun startSignUpActivity() {
        val intent = Intent(this, SignUpActivity::class.java)

        startActivity(intent)
        finish()
    }

    override fun initObservers() {
        viewModel.apiStatus.observe(this, Observer {
            handleAPIStatus(it)
        })

        viewModel.loginResponse.observe(this, Observer {
            handleLoginResponse()
        })

        viewModel.username.observe(this, Observer {
            if(it.isNotEmpty()) {
                etUsername.error = null
            }
        })

        viewModel.password.observe(this, Observer {
            if(it.isNotEmpty()) {
                etPassword.error = null
            }
        })
    }

    private fun handleLoginResponse() {
        val preferences = Preferences.getInstance(this)

        preferences.saveUserName(viewModel.loginResponse.value?.name)
    }

    private fun handleAPIStatus(it: ApiStatus) {
        CommonUtils.processAPIStatus(it, this)

        if(it == ApiStatus.SUCCESS) {
            startHomeScreen()
        } else if(it == ApiStatus.FAILURE) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.loginResponse.value?.message)
        }
    }

    private fun CharSequence.isValidName(): Boolean =
        Pattern.compile(Constants.REGEX_EMAIL_VALIDATOR).matcher(this.trim()).find()

    private fun CharSequence?.isValidPassword(): Boolean {
        return this?.trim()?.isNotEmpty()!! && this.trim().length >= Constants.VALUE_PASSWORD_LENGTH
    }

    companion object {
       private const val TAG = "SignInActivity"
    }
}

