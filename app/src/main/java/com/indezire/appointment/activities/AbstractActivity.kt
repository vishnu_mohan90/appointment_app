package com.indezire.appointment.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.indezire.appointment.R

abstract class AbstractActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
//        supportActionBar?.elevation = 0.0f


        initLayout()
        initListeners()
        initObservers()
    }

    abstract fun initObservers()

    abstract fun initListeners()

    abstract fun initLayout()
}