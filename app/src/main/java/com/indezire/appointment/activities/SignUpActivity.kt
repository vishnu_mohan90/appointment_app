package com.indezire.appointment.activities

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.indezire.appointment.R
import com.indezire.appointment.databinding.ActivitySignupBinding
import com.indezire.appointment.enums.ApiStatus
import com.indezire.appointment.helpers.CommonUtils
import com.indezire.appointment.helpers.Constants
import com.indezire.appointment.viewmodels.SignUpViewModel
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.regex.Pattern

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SignUpActivity : AbstractActivity() {
    private lateinit var viewModel:  SignUpViewModel

    override fun initLayout() {
        val binding: ActivitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.hide()

        viewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    override fun initListeners() {
        btSend.setOnClickListener {
            validateAndSignUp()
        }

        tvSignInHere.setOnClickListener {
            startSignInActivity()
        }
    }

    private fun startSignInActivity() {
        val intent = Intent(this, SignInActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun validateAndSignUp() {
        val isValid = isValidData()

        if (isValid) {
            viewModel.signup()
        }
    }

    private fun isValidData(): Boolean {
        var isValid = true

        if(!viewModel.userName.value?.isValidName()!!) {
            isValid = false
            etUsername.error = "Please enter a valid username"
            etUsername.requestFocus()
        }  else if (etConfirmPassword.error != null) {
            isValid = false
            etConfirmPassword.error = "Password do not match"
            etConfirmPassword.requestFocus()
        } else if (!viewModel.emailId.value?.isValidEmail()!!) {
            isValid = false
            etMailId.error = "Please enter a valid mail id"
            etMailId.requestFocus()
        } else if (!viewModel.phoneNumber.value?.isPhoneNumber()!!) {
            isValid = false
            etPhoneNumber.error = "Please enter a valid phone number"
            etPhoneNumber.requestFocus()
        } else if (!viewModel.businessId.value?.isValidBusinessId()!!) {
            isValid = false
            etBusinessId.error = "Please enter a valid business id"
            etBusinessId.requestFocus()
        } else if (!viewModel.password.value?.isValidPassword()!!) {
            isValid = false
            etPassword.error = "Please enter a valid password"
            etPassword.requestFocus()
        }

        return isValid
    }

    override fun initObservers() {
        viewModel.userName.observe(this, Observer {
            if(it.isNotEmpty() && etUsername.error != null) {
                etUsername.error = null
            }
        })

        viewModel.password.observe(this, Observer {
            if(it.isNotEmpty() && etPassword.error != null) {
                etPassword.error = null
            }
        })

        viewModel.confirmPassword.observe(this, Observer {
            if(it.isNotEmpty() && viewModel.password.value == it) {
                etPassword.error = null
            }
        })

        viewModel.emailId.observe(this, Observer {
            if(it.isNotEmpty() && etMailId.error != null) {
                etMailId.error = null
            }
        })

        viewModel.phoneNumber.observe(this, Observer {
            if(it.isNotEmpty() && etPhoneNumber.error != null) {
                etPhoneNumber.error = null
            }
        })

        viewModel.businessId.observe(this, Observer {
            if(it.isNotEmpty() && etBusinessId.error != null) {
                etBusinessId.error = null
            }
        })

        viewModel.apiStatus.observe(this, Observer {
            handleAPIResponse(it)
        })
    }

    private fun handleAPIResponse(apiStatus: ApiStatus) {
        CommonUtils.processAPIStatus(apiStatus, this)

        if(apiStatus == ApiStatus.SUCCESS) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.apiResponse.value?.message) {
                startHomeScreenActivity()
            }


        } else if(apiStatus == ApiStatus.FAILURE) {
            CommonUtils.showSnackBar(clSnackBarMessage, viewModel.apiResponse.value?.message)
        }
    }

    private fun startHomeScreenActivity() {
        val intent = Intent(this, HomeScreenActivity::class.java)

        startActivity(intent)
        finish()
    }

    private var pattern: Pattern = Pattern.compile(Constants.REGEX_PHONE_NUMBER_VALIDATOR)
    private fun CharSequence.isPhoneNumber(): Boolean = pattern.matcher(this).find()
    private fun CharSequence.isValidName(): Boolean =
        Pattern.compile(Constants.REGEX_NAME_VALIDATOR).matcher(this.trim()).find()
    private fun CharSequence.isValidEmail(): Boolean =
        Pattern.compile(Constants.REGEX_EMAIL_VALIDATOR).matcher(this.trim()).find()
    private fun CharSequence.isValidBusinessId(): Boolean =
        Pattern.compile(Constants.REGEX_BUSINESS_ID_VALIDATOR).matcher(this.trim()).find()
    private fun CharSequence?.isValidPassword(): Boolean {
        return this?.trim()?.isNotEmpty()!! && this.trim().length >= Constants.VALUE_PASSWORD_LENGTH
    }
    companion object {
       private const val TAG = "SignUpActivity"
    }
}