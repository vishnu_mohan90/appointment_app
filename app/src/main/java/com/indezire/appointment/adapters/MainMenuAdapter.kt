package com.indezire.appointment.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.indezire.appointment.R
import com.indezire.appointment.interfaces.IMainMenuClickListener
import com.indezire.appointment.models.MenuItem
import kotlinx.android.synthetic.main.single_item_main_menu.view.*

class MainMenuAdapter(val context: Context,
                      private val menuItems: ArrayList<MenuItem>,
                      val mainMenuClickListener: IMainMenuClickListener):
    RecyclerView.Adapter<MainMenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_main_menu, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val menuItem = menuItems[position]
        
        holder.bind(menuItem)
        holder.rootView.isSelected = menuItem.isSelected

        if(menuItem.isSelected) {
            holder.rootView.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            holder.rootView.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black))
        }

        holder.rootView.setOnClickListener {
            handleMenuItemSelected(position)
        }
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }

    private fun handleMenuItemSelected(position: Int) {
        for(menuItem in menuItems) {
            menuItem.isSelected = false
        }
        menuItems[position].isSelected = true
        mainMenuClickListener.onMenuItemClicked(menuItems[position])
        notifyDataSetChanged()
    }

    class ViewHolder(val rootView: View): RecyclerView.ViewHolder(rootView) {
        fun bind(menuItem: MenuItem) {
            rootView.tvTitle.text = menuItem.title
        }
    }


}