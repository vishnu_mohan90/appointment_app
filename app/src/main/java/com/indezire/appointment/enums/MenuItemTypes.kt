package com.indezire.appointment.enums

enum class MenuItemTypes(value: Int) {
    MY_APPOINTMENTS(1),
    BOOK_APPOINTMENTS(2),
    ALERTS(3),
    LOGOUT(4)
}