package com.indezire.appointment.enums

enum class ApiStatus(status: Int) {
    LOADING(1),
    SUCCESS(2),
    FAILURE(3),
    NO_NETWORK(4),
    ERROR_RESPONSE(5)
}