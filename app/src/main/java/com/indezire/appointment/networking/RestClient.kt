package com.indezire.appointment.networking

import android.content.Context
import com.google.gson.GsonBuilder
import com.indezire.appointment.helpers.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Gladiator on 23/09/2017.
 */
class RestClient() {
    var apiInterface: APIInterface

    init {
        val gson = GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .create()
        val interceptor = HttpLoggingInterceptor()
        val client = OkHttpClient.Builder()
            .connectTimeout(180, TimeUnit.SECONDS)
            .readTimeout(180,TimeUnit.SECONDS).
            addInterceptor(HTTPInterceptor()).
            addInterceptor(interceptor).build()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        apiInterface = retrofit.create(APIInterface::class.java)
    }

    companion object {
        private lateinit var restClient: RestClient

        fun getRestClient():RestClient {
            if(!::restClient.isInitialized) {
                restClient = RestClient()
            }
            return restClient
        }
    }
}