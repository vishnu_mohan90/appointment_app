package com.indezire.appointment.networking

import com.indezire.appointment.models.networking.APIResponse
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

class CallbackResponse(): Callback {
    override fun onResponse(call: Call, response: Response) {

    }

    override fun onFailure(call: Call, e: IOException) {

    }
}