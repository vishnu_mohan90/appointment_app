package com.indezire.appointment.networking;

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer
import org.json.JSONException

import org.json.JSONObject
import java.io.IOException


/**
 * Created by Vishnu Mohan on 10/25/2017.
 */

class HTTPInterceptor (): Interceptor {
    private lateinit var token: String

    init {
//        token = preferences.getAuthToken();
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request: Request
        val subtype: String
        var requestBody: RequestBody?
        val builder: Request.Builder
        val original = chain.request()

        //        token = preferences.getAuthToken();
        builder = original.newBuilder()

//        if (!token.equals("")) {
//            builder = builder.
//                    addHeader("Authorization", "token " + token);
//        }
        request = builder.method(original.method(), original.body()).build()
        requestBody = request.body()
        subtype = requestBody?.contentType()?.subtype() ?: ""

        if(subtype.contains("json")) {
            requestBody = processApplicationJsonRequestBody(requestBody);
        }

        if (requestBody != null) {
            val requestBuilder = request.newBuilder()
            request = requestBuilder
                .post(requestBody)
                .build()
        }

        return chain.proceed(request)
    }

    private fun processApplicationJsonRequestBody(requestBody: RequestBody? ): RequestBody? {
        val customReq: String = bodyToString(requestBody)
        val newJsonObject = JSONObject()
        try {
            val obj = JSONObject(customReq)
            newJsonObject.put("params", obj)
            return RequestBody.create(requestBody?.contentType(), newJsonObject.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return null
    }

    private fun bodyToString(request: RequestBody?): String {
        return try {
            val buffer = Buffer()
            if (request != null) request.writeTo(buffer) else return ""
            buffer.readUtf8()
        } catch (e: IOException) {
            "did not work"
        }
    }

}
