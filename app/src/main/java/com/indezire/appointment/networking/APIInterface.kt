package com.indezire.appointment.networking;

import com.indezire.appointment.models.networking.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Gladiator on 23/09/2017.
 */

public interface APIInterface {
    @POST("login")
    fun login(@Body loginRequest: LoginRequest): Call<APIResponse<LoginResponse>>

    @POST("forgetPassword")
    fun verifyEmail(@Body verifyEmailRequest: VerifyEmailRequest): Call<APIResponse<VerifyEmailResponses>>

    @GET("user/resetPassword")
    fun resetPassword(@Query("id") resetPasswordRequest: ResetPasswordRequest): Call<APIResponse<ResetPasswordResponse>>

    @POST("user/signup")
    fun signup(@Body signupRequest: SignupRequest): Call<APIResponse<SignupResponse>>

//    @GET("/users/employees/")
//    Call<EmployeeListCallback> getEmployees(@Query("propertyId") int propertyId);
//
//    @GET("/properties/bookings/")
//    Call<BookingReportModel> getBookingReport(@Query("timeStamp") String timeStamp,
//                                              @Query("propertyId") int propertyId);
//
//    @GET("/properties/bookings/")
//    Call<ArrayList<BookingReportModel>> getBookingReport(@Query("startDate") String startDate, @Query("endDate") String endDate,
//                                                         @Query("propertyId") int propertyId);
//
//    @Headers({"Accept: application/json"})
//    @POST("/properties/bookings/")
//    Call<BookingReportCallback> saveBookingReport(@Body BookingReportModel reportRequest);
//
//    @Multipart
//    @POST("/instructions/")
//    Call<Instruction> saveInstruction(@Part ArrayList<MultipartBody.Part> parts,
//                                      @Part("title") RequestBody title,
//                                      @Part("instruction") RequestBody instruction,
//                                      @Part("propertyId") RequestBody propertyId,
//                                      @Part("type") RequestBody type);
//
//    @GET(Constants.GOOGLE_PLACES_URL)
//    Call<GoogleDirectionsCallback> getDirections(@Query("origin") String origin,
//                                                 @Query("destination") String destination,
//                                                 @Query("key") String key);
//
//    @Headers({"Accept: application/json"})
//    @POST("/users/fcm/")
//    Call<ResponseBody> saveFCMToken(@Body LoginActivity.FcmToken token);
//
//    @Headers("Accept: application/json")
//    @POST("/instructions/change-status/")
//    Call<ResponseBody> changeInstructionStatus(@Body InstructionDetails.CallStatus status);
//
//    @Multipart
//    @POST("/instructions/comment/")
//    Call<InstructionComment> saveComment(@Part("comment") RequestBody comment,
//                                         @Part("instructionId") RequestBody instructionId,
//                                         @Part MultipartBody.Part image);
//
//    @Multipart
//    @POST("/instructions/comment/")
//    Call<InstructionComment> saveComment(@Part("comment") RequestBody comment,
//                                         @Part("instructionId") RequestBody instructionId);
//
//    @POST("users/employees/attendance/")
//    Call<ResponseBody> saveAttendance(@Body ArrayList<EmployeeDetailsGM.EmployeeChild> children);
//
//    @Headers("Accept: application/json")
//    @POST("properties/property-visit/")
//    Call<ResponseBody> saveLastVisit(@Body GeoFenceTransitionService.VisitProperty property);
//
//    @GET("properties/property-visit/")
//    Call<GetLastVisitCallback> getLastVisitsList(@Query("propertyId") int propertyId, @Query("userRole") int userRole);
//
//    @GET("/properties/search/")
//    Call<GetPlacesCallback> searchProperty(@Query("query") String query);
}