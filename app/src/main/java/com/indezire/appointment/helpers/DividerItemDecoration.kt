package com.indezire.appointment.helpers

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class DividerItemDecoration: RecyclerView.ItemDecoration() {
    private val offset = 20
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        val index = parent.getChildLayoutPosition(view)

        if(index % 2 == 0) {
            outRect.right = offset
        }

        outRect.bottom = offset
    }
}