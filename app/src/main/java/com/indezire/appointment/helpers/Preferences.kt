package com.indezire.appointment.helpers

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class Preferences(context: Context) {
    private var sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private lateinit var editor: SharedPreferences.Editor

    init {
        editor = sharedPreferences.edit()
    }

    fun saveUserMailId(mailId: String) {
        editor.putString(Constants.KEY_USER_MAIL_ID, mailId)
        editor.commit()
    }

    fun getUserMailId(): String {
        return sharedPreferences.getString(Constants.KEY_USER_MAIL_ID, "") ?: ""
    }

    fun setLoggedIn() {
        editor.putBoolean(KEY_IS_LOGGED_IN, true)
        editor.commit()
    }

    fun isLoggedIn(): Boolean {
        return sharedPreferences.getBoolean(KEY_IS_LOGGED_IN, false)
    }

    fun saveUserName(name: String?) {
        editor.putString(KEY_USER_NAME, name)
        editor.commit()
    }

    companion object {
        private const val TAG = "Preferences"
        private lateinit var preferences: Preferences

        private const val KEY_USER_NAME     = "user_name"
        private const val KEY_IS_LOGGED_IN  = "isLoggedIn"

        fun getInstance(context: Context): Preferences {
            if(!::preferences.isInitialized) {
                preferences = Preferences(context)
            }

            return preferences
        }
    }

}