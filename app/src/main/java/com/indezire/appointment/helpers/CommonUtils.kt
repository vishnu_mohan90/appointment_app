package com.indezire.appointment.helpers

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.indezire.appointment.R
import com.indezire.appointment.enums.ApiStatus

object CommonUtils {
    private lateinit var dialog: Dialog

    fun processAPIStatus(status: ApiStatus, context: Context) {
        when(status) {
            ApiStatus.LOADING -> {
                showProgressBarDialog(context)
            }

            ApiStatus.SUCCESS -> {
                dialog.dismiss()
            }

            else -> {
                dialog.dismiss()
            }
        }
    }

    private fun showProgressBarDialog(context: Context) {
        dialog = Dialog(context)
        if (!dialog.isShowing) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.progress_bar_dialog_layout)
            dialog.show()

            val window = dialog.window
            window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun showSnackBar(view: View, message: String?, callback: () -> Unit) {
        val snackBar = getSnackBarInstance(view, message)

        snackBar.addCallback(object: Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                callback()
            }
        })

        snackBar.show()
    }

    fun showSnackBar(view: View, message: String?) {
        if (message != null) {
            val snackBar = getSnackBarInstance(view, message)
            snackBar.show()
        }
    }

    private fun getSnackBarInstance(view: View, message: String?): Snackbar {
        val color: Int = Color.WHITE
        val snackBar = Snackbar.make(view, message ?: "", Snackbar.LENGTH_LONG)
        val sbView = snackBar.view
        val textView = sbView.findViewById(R.id.snackbar_text) as TextView

        textView.setTextColor(color)

        return snackBar
    }
}