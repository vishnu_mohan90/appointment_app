package com.indezire.appointment.helpers

object Constants {
    const val VALUE_PASSWORD_LENGTH             = 8

    const val BASE_URL                          = "http://3.131.153.245:8073/api/v1/"
    const val API_STATUS_SUCCESS                = "success"

    const val KEY_USER_MAIL_ID                  = "mail_id"

    const val REGEX_NAME_VALIDATOR              = "^[a-zA-Z0-9. ]*\$"
    const val REGEX_PHONE_NUMBER_VALIDATOR      = "^([\\-\\s]?)?[0]?(91)?[123456789]\\d{9}\$"
    const val REGEX_PASSWORD_VALIDATOR          = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%^&+=])(?=\\\\S+\$).{4,}\$\n"
    const val REGEX_EMAIL_VALIDATOR             = "[a-zA-Z0-9._-]+@[a-z]+\\.[a-z]+"
    const val REGEX_BUSINESS_ID_VALIDATOR       = "^[a-zA-Z0-9_]*$"
}