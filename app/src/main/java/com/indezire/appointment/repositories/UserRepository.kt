package com.indezire.appointment.repositories

import com.indezire.appointment.helpers.Constants
import com.indezire.appointment.models.networking.*
import com.indezire.appointment.networking.RestClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object UserRepository {
    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)
    private val restClient = RestClient.getRestClient()

    fun signup (signupRequest: SignupRequest,
                onResult: (isSuccess: Boolean, response: SignupResponse?) -> Unit) {
        scope.launch {
            restClient.apiInterface.signup(signupRequest).enqueue(object: Callback<APIResponse<SignupResponse>> {
                override fun onResponse(call: Call<APIResponse<SignupResponse>>,
                                        response: Response<APIResponse<SignupResponse>>) {
                    onResult(response.body()?.result?.status ==
                            Constants.API_STATUS_SUCCESS, response.body()?.result)
                }

                override fun onFailure(call: Call<APIResponse<SignupResponse>>, t: Throwable) {
                    onResult(false, null)
                }
            })
        }
    }

    fun login (
        loginRequest: LoginRequest,
        onResult: (isSuccess: Boolean, response: LoginResponse?) -> Unit) {
        scope.launch {
            restClient.apiInterface.login(loginRequest)
                .enqueue(object : Callback<APIResponse<LoginResponse>> {
                    override fun onResponse(
                        call: Call<APIResponse<LoginResponse>>,
                        response: Response<APIResponse<LoginResponse>> ) {
                        onResult(response.body()?.result?.status ==
                                Constants.API_STATUS_SUCCESS, response.body()?.result)
                    }

                    override fun onFailure(call: Call<APIResponse<LoginResponse>>, t: Throwable) {
                        onResult(false, null)
                    }
                })
        }
    }

    fun verifyEmail(emailRequest: VerifyEmailRequest,
                    onResult: (isSuccess: Boolean, response: VerifyEmailResponses?) -> Unit) {
        scope.launch {
            restClient.apiInterface.verifyEmail(emailRequest).enqueue(object: Callback<APIResponse<VerifyEmailResponses>> {
                override fun onResponse(call: Call<APIResponse<VerifyEmailResponses>>,
                                        response: Response<APIResponse<VerifyEmailResponses>> ) {
                    onResult(response.body()?.result?.status ==
                            Constants.API_STATUS_SUCCESS, response.body()?.result)
                }

                override fun onFailure(call: Call<APIResponse<VerifyEmailResponses>>, t: Throwable) {
                    onResult(false, null)
                }
            })
        }
    }

    fun resetPassword(resetPasswordRequest: ResetPasswordRequest,
                      onResult: (isSuccess: Boolean, response: ResetPasswordResponse?) -> Unit) {
        scope.launch {
            restClient.apiInterface.resetPassword(resetPasswordRequest).enqueue(object: Callback<APIResponse<ResetPasswordResponse>> {
                override fun onResponse(call: Call<APIResponse<ResetPasswordResponse>>,
                                        response: Response<APIResponse<ResetPasswordResponse>>) {
                    onResult(response.body()?.result?.status ==
                            Constants.API_STATUS_SUCCESS, response.body()?.result)
                }

                override fun onFailure(call: Call<APIResponse<ResetPasswordResponse>>,
                                       t: Throwable) {
                    onResult(false, null)
                }
            })
        }
    }
}