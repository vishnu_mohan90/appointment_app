package com.indezire.appointment.models.networking
import com.google.gson.annotations.SerializedName


data class ResetPasswordRequest(
    @SerializedName("login")
    var login: String,
    @SerializedName("otp")
    var otp: String,
    @SerializedName("password")
    var password: String
)