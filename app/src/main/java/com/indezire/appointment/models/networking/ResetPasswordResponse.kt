package com.indezire.appointment.models.networking
import com.google.gson.annotations.SerializedName


data class ResetPasswordResponse (
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
)