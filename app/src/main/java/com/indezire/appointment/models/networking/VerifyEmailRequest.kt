package com.indezire.appointment.models.networking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VerifyEmailRequest(
    @SerializedName("login")
    @Expose
    var emailId: String
)