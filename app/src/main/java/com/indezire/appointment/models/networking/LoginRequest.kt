package com.indezire.appointment.models.networking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginRequest {
    @SerializedName("db")
    @Expose
    var database = ""
    @SerializedName("login")
    @Expose
    var username = ""
    @SerializedName("password")
    @Expose
    var password = ""
}