package com.indezire.appointment.models.networking
import com.google.gson.annotations.SerializedName


data class SignupResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
)