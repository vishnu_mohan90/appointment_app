package com.indezire.appointment.models.networking
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VerifyEmailResponses(
    @SerializedName("message")
    @Expose
    var message: String,
    @SerializedName("otp")
    @Expose
    var otp: Int,
    @SerializedName("status")
    @Expose
    var status: String
)