package com.indezire.appointment.models

import com.indezire.appointment.enums.MenuItemTypes

class MenuItem (val type: MenuItemTypes, val title: String, val icon: Int) {
    var isSelected = false
    companion object {
        fun getMenuItems(): ArrayList<MenuItem> {
            val menuItems = ArrayList<MenuItem>()

            menuItems.add(MenuItem(MenuItemTypes.BOOK_APPOINTMENTS, "Book An\nAppointment", 0))
            menuItems.add(MenuItem(MenuItemTypes.MY_APPOINTMENTS, "My\nAppointments", 0))
            menuItems.add(MenuItem(MenuItemTypes.ALERTS, "Alerts", 0))
            menuItems.add(MenuItem(MenuItemTypes.LOGOUT, "Log Out", 0))

            return menuItems
        }
    }
}