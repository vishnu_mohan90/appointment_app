package com.indezire.appointment.models.networking

import com.google.gson.annotations.SerializedName

class SignupRequest (
    @SerializedName("email")
    var email: String,
    @SerializedName("mobile")
    var mobile: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("password")
    var password: String
)