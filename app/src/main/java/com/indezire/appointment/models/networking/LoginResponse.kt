package com.indezire.appointment.models.networking

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("status")
    @Expose
    val status: String = ""

    @SerializedName("message")
    @Expose
    val message: String = ""

    @SerializedName("status_code")
    @Expose
    val statusCode: Int = -1

    @SerializedName("uid")
    @Expose
    val uid: Int = -1

    @SerializedName("user_type")
    @Expose
    val userType: String = ""

    @SerializedName("photo")
    @Expose
    val photo: String = ""

    @SerializedName("partner_id")
    @Expose
    val partnerId: Int = -1

    @SerializedName("name")
    @Expose
    val name: String = ""

    @SerializedName("email")
    @Expose
    val email: String = ""

    @SerializedName("mobile")
    @Expose
    val mobile: String = ""

    @SerializedName("session_id")
    @Expose
    val sessionId: String = ""
}