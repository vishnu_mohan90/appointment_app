package com.indezire.appointment.models.networking

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class APIResponse<T> {
    @SerializedName("jsonrpc")
    @Expose
    val jsonrpc: String = ""

    @SerializedName("id")
    @Expose
    val id: Int = 0

    @SerializedName("result")
    @Expose
    val result: T? = null
}